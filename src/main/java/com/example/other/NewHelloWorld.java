package com.example.other;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NewHelloWorld {

  @RequestMapping("/")
  public String get(){
    return "Happy Monday From Other Project";
  }
}
